# uniconv-server

Service de conversion des fichiers au format doc en pdf

## Pour développer

Pour lancer le serveur en local:
```bash
bin/python src/runServeurHTTPUnoconv.py
ou
```bash
bin/python src/runServeurHTTPUnoconv.py --addr=127.0.0.1 --port=80
```

Remarque: la redirection de port est aussi à configurer dans l'appel à bin/python.


## Pour construire

Construire l'image du docker
```bash
./build
```

## Pour lancer l'image

Pour lancer le docker
```bash
./run
```