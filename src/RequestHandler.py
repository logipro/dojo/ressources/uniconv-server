from http.server import BaseHTTPRequestHandler
import logging
from subprocess import Popen, PIPE, STDOUT
class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200, message='OK')
        self.end_headers()
    def do_POST(self):
        logging.info("Requete POST reçue")